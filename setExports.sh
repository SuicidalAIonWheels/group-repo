#!/bin/bash

IP="$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')"
IP=${IP[0]}
IP=${IP#*inet}
IP=${IP#*inet}
IP=${IP#*addr:}
IP=${IP#*addr:}
IP=${IP#*inet }
echo ${IP}
echo "export ROS_MASTER_URI=http://192.168.43.$1:11311"
echo `export ROS_MASTER_URI=http://192.168.43.$1:11311`
echo "export ROS_HOSTNAME=${IP}"
echo `export ROS_HOSTNAME=${IP}`
echo "export ROS_IP=${IP}"
echo `export ROS_IP=${IP}`

#source devel/setup.bash
