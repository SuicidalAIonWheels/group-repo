#!/usr/bin/env python

import rospy
import cv2
import numpy as np
from math import sqrt
from std_msgs.msg import String
from std_msgs.msg import Float32
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class line_detector:
    """
    Detects a white line in the camera image and returns how far off it is from a vertical line in the image center.
    This vertical line is characterised by being collinear to (0,1) and the x-coordinates of its points
    shall be at the image center. This class returns the angle to (0,1) and x - x_image_center at the image bottom.
    """

    def __init__(self):
        self.image_pub = rospy.Publisher("/line_detector/lines_drawn", Image, queue_size = 1) # publish camera image with lines drawn so we can see what this ends up doing
        self.blackwhite_image_pub = rospy.Publisher("/line_detector/blackwhite", Image, queue_size = 1) # publish black_white image
        self.bridge = CvBridge()
        #camera_sub = rospy.Subscriber("/camera/color/image_raw", Image, callback, queue_size=1)
        # subscribing to the camera will be handled by the line_follower script

    def RANSAClinefit(self, points, maxNumIter, maxDistance, requiredInlierPercentage):
        """ 
        Fits a line to the given points in an image using RANSAC.
        Note that it assumes that the x-axis goes from left to right, y from bottom to top.
        Just like CV2's fitline() function, returns a collinear vector and a point on the line
        (since it applies fitline() to the determined selection)
        """
        iteration = 0
        fitThesePoints = None
        d = requiredInlierPercentage * points.shape[0]
        
        while iteration < maxNumIter:
            firstPointIndex = np.random.randint(0, points.shape[0])
            secondPointIndex = np.random.randint(0, points.shape[0])
                
            firstPoint = points[firstPointIndex, :]
            secondPoint = points[secondPointIndex, :]
            
            # fit line to the two selected points
            if firstPoint[0] != secondPoint[0]: # ignore bad pair
                m = (firstPoint[1] - secondPoint[1]) / float(firstPoint[0] - secondPoint[0])
                b = firstPoint[1] - m * firstPoint[0]
            
                # determine if we have enough inliers
                inliers = []
                for i in range(points.shape[0]):
                    candidate = points[i,:]
                    distance = abs(m * candidate[0] - candidate[1] + b) / sqrt(m * m + 1)
                    if distance < maxDistance:
                        inliers.append(candidate)
                
                if len(inliers) >= d:
                    fitThesePoints = np.array(inliers)
                    break
                    
            iteration += 1
                
        if fitThesePoints is None:
            print('Unable to find good fit!')
            return None # we end up not changing the steering angle and wait for the next image
            
        print("fitting points selection")
        return cv2.fitLine(fitThesePoints, cv2.DIST_L2, 0, 0.01, 0.01)

    def getLineError(self, image):
        """
        Returns the angle of the line in the image to (0,1)
        and x - x_image_center where x is the x-coordinate of the image bottom point on the line
        """
        try:
            cv_image = self.bridge.imgmsg_to_cv2(image, "bgr8")
        except CvBridgeError as e:
            print(e)
            
        x_image_center = int(cv_image.shape[1] / 2)
        y_image_bottom = cv_image.shape[0] - 1 # largest y-coordinate

        # make it gray
        gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        
        # turn black/white
        bi_gray_max = 255
        bi_gray_min = 240
        ret, thresh1 = cv2.threshold(gray, bi_gray_min, bi_gray_max, cv2.THRESH_BINARY);
        
        try:
            self.blackwhite_image_pub.publish(self.bridge.cv2_to_imgmsg(thresh1, "mono8"))
        except CvBridgeError as e:
            print(e)
        
        # we scan the image horizontally looking for white boundary points
        pointsOnLineList = []
        for y in range(thresh1.shape[0]):
            x = thresh1.shape[1] - 1
            leftBoundaryX = None
            rightBoundaryX = None
            
            # look for left boundary
            while x >= 0:
                if thresh1[y][x] > 0: # white pixel
                    rightBoundaryX = x
                    break
                x -= 1
            
            if rightBoundaryX is not None:
                # look for right boundary
                while x >= 0:
                    if thresh1[y][x] == 0: # black pixel
                        leftBoundaryX = x + 1
                        break
                    x -= 1
                            
            # add point on line if we have found the boundaries
            if leftBoundaryX is not None:
                pointsOnLineList.append([(leftBoundaryX + rightBoundaryX) / float(2), y])

        pointsOnLine = np.array(pointsOnLineList)

        # fit line
        RANSAC_ret = self.RANSAClinefit(pointsOnLine, 20, 10, 0.6) # parameters determined by trial and error
        if RANSAC_ret is not None: 
            # need line to proceed
            # draw line into the image and publish it
            cv2.line(cv_image, (int(RANSAC_ret[2,0] - 500 * RANSAC_ret[0,0]), int(RANSAC_ret[3,0] - 500 * RANSAC_ret[1,0])), (int(RANSAC_ret[2,0] + 500 * RANSAC_ret[0,0]), int(RANSAC_ret[3,0] + 500 * RANSAC_ret[1,0])), (0,255,0), 3)
            try:
                self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
            except CvBridgeError as e:
                print(e)
              
            # Since we're looking for the angle to (0,1) and want the collinear vector to point upwards,
            # we just compute arccos(abs(y)) where y is the y-component of the collinear vector from RANSAC.
            # We'd like the angle to be in the range (-pi, pi) and adjust the sign based on the x-offset
            return_angle = np.arccos(abs(RANSAC_ret[1,0]))
            
            # Get x-offset at the image bottom. For this, determine the x-coordinate of the line at y_image_bottom first.
            if RANSAC_ret[1,0] == 0:
                # this means the line is horizontal and the car screwed up royally, return infinity then
                if RANSAC_ret[0,0] > 0:
                    return np.pi/2, np.inf
                else:
                    return -np.pi/2, np.ninf
                
            t = (y_image_bottom - RANSAC_ret[3,0]) / RANSAC_ret[1,0]
            x_line_at_bottom = RANSAC_ret[2,0] + t * RANSAC_ret[0,0]
            
            # if the x-offset is positive the line leans to the right relative of a vertical line at the image center, so we want the angle to be negative
            x_offset = x_line_at_bottom - x_image_center
            if x_offset > 0:
                return_angle = -return_angle
            
            return return_angle, x_offset
        else:
            print("unable to find line, trying next image")
            return None, None
