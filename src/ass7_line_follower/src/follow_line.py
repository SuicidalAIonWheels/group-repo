#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Image
import numpy as np

from line_detector import line_detector
from std_msgs.msg import Int16, UInt8

speed_value = 130
max_steering_angle_left = -0.7
max_steering_angle_right = -max_steering_angle_left
min_command_angle = 0
max_command_angle = 179
KP = 0.75 * 0.7 / 400.0 # P-controller factor, determined experimentally
# we start by supposing that an x-offset of 400 shall correspond to the minimum steering angle
# (positive offset means we are to the right of the line and have to steer left)
# factor of 0.75 seemed to work good enough

def callback(data):
    """
    Callback function for subscribing to the camera feed. Detects the white line in the image
    and adjusts steering accordingly.
    """
    global previous_errors, max_steering_angle, min_steering_angle, min_command_angle, max_command_angle, KP, detector
    
    angle_offset_rad, x_offset = detector.getLineError(data) # ended up not using the angle
    # if we for some reason failed to compute the offset we just keep the steering as is and wait for the next camera image
    if x_offset is not None:
        # get the desired steering angle based on the average previous errors
        if np.isinf(x_offset):
            print("Car is perpendicular to the line and doesn't know how to deal with that!")
        else:
            previous_errors = np.append(previous_errors[1:], [x_offset])
            desired_steering_angle = KP * np.average(previous_errors)
            
            # publish command angle; we use a simple linear transformation
            desired_command_angle = max_steering_angle_left + (desired_steering_angle - max_steering_angle_left) * (max_command_angle - min_command_angle) / (max_steering_angle_right - max_steering_angle_left)
            pub_steering.publish(desired_command_angle)
    
def stop_car():
    # This was an attempt to get the car to stop upon termination that did not work. The message is printed but the speed change isn't published, requiring manual resets
    pub_speed.publish(0)
    pub_steering.publish(90)
    rospy.sleep(1)
    print("stopped the car")

# setup
rospy.init_node('ass7_line_follower')
camera_sub = rospy.Subscriber("/camera/color/image_raw", Image, callback, queue_size=1)
detector = line_detector() # looks for the line
previous_errors = np.zeros(1)

# start driving
pub_speed = rospy.Publisher("/speed", Int16, queue_size=100, latch=True)
pub_steering = rospy.Publisher("/steering", UInt8, queue_size=100, latch=True)
pub_speed.publish(speed_value) # start driving
pub_steering.publish(90) # set initial steering to neutral

rospy.spin() # track white line and adjust steering as needed
    
rospy.on_shutdown(stop_car)
