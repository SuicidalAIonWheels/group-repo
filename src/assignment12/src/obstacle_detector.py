#!/usr/bin/env python

import numpy as np
import rospy
import math
from std_msgs.msg import ColorRGBA
from sensor_msgs.msg import LaserScan

from predict_closest_point import get_closest_point

class ObstacleDetector:
    """
    Detects obstacles on both lanes. Uses ColorRGBA to output the distance to the closest obstacle
    on lane 1 at "r" and on lane 2 at "g".
    """
    def __init__(self):
        # parameter
        self.largest_distance_to_be_on_lane = 0.05  # how far away a point can be from the lane centre to be considered on it
        self.distance_front_axis_lidar = 0.06 # distance between middle point of front axis and centre of LIDAR
        
        # track car position and heading (note that car position should refer to midpoint of front axis)
        self.current_x = 0.0
        self.current_y = 0.0
        self.heading_x = 1.0
        self.heading_y = 0.0
        
        # publisher and subscriber
        self.position_sub = rospy.Subscriber("/car_tracker/position", ColorRGBA, self.get_car_position_data, queue_size=1)
        self.lazor_sub = rospy.Subscriber("/scan", LaserScan, self.publish_nearest_obstacle_distances, queue_size=1)
        
        self.distances_pub = rospy.Publisher("/obstacle_detector", ColorRGBA, queue_size=1)
        
    def get_car_position_data(self, data):
        """
        Update current car position and heading.
        """
        self.current_x = data.r
        self.current_y = data.g
        self.heading_x = data.b
        self.heading_y = data.a
        
    def compute_distance(self, point1, point2):
        """
        Simple euclidean distance of two 2D points.
        """
        return  math.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)
        
    def publish_nearest_obstacle_distances(self, data):
        """
        Computes and publishes distance to closest obstacle on lane 1 and 2.
        """
        smallest_distance_on_lane1 = float("inf")
        smallest_distance_on_lane2 = float("inf")
        
        # lock current position and rotation
        car_pos = [self.current_x, self.current_y]
        heading_x = self.heading_x
        heading_y = self.heading_y
        
        # get heading angle relative to (1,0) heading ranging from -pi to pi
        heading_angle = math.atan2(heading_y, heading_x)
        
        # car positions on lanes
        car_x_on_lane1, car_y_on_lane1, _ = get_closest_point(car_pos, 1, 0.0)
        car_x_on_lane2, car_y_on_lane2, _ = get_closest_point(car_pos, 2, 0.0)

        car_on_lane1 = [car_x_on_lane1, car_y_on_lane1]
        car_on_lane2 = [car_x_on_lane2, car_y_on_lane2]
        
        # consider every data point
        distances = np.asarray(data.ranges)
        angle = data.angle_min # angle of current point
        """
        The angle of zero is at the back of the car, +- pi is at the front, -pi/2 is the left, pi/2 the right side.
        We only consider points ahead of the car, so angles with absolute value above 3pi/4.
        
        Now it gets a bit tricky: In LIDAR coordinates the car points in direction (-1,0). We can make the car point into
        (1,0) direction by adding pi to the angle such that the scan point is at distance * [cos(angle + pi), sin(angle + pi)].
        This vector has its origin at the LIDAR, but we want it to be anchored at the front axis. The vector pointing from
        the midpoint of the front axis to the LIDAR in the rotated LIDAR coordinates is (-distance_front_axis_lidar, 0) which we just
        add to the scan point to get its position in car coordinates (vector from car to point = vector from car to lidar + lidar to point). 
        Transforming this into global coordinates then only requires and adjustment of heading as determined by the car's heading 
        and a shift to the car coordinates.
        """
        angle_cutoff = 3.0 * math.pi / 4.0 # smallest absolute angle to consider
        for distance in distances:
            if data.range_min < distance and distance < data.range_max: # documentation says to discard points outside of this
                if abs(angle) >= angle_cutoff:
                    # compute global coordinates of the current data point:
                    # position relative to LIDAR
                    scan_position_lidar = [distance * math.cos(angle + math.pi), distance * math.sin(angle + math.pi)]
                    
                    # position relative to car
                    scan_position_car = [scan_position_lidar[0] - self.distance_front_axis_lidar, scan_position_lidar[1]]

                    # rotate according to car heading
                    heading_cos = math.cos(heading_angle)
                    heading_sin = math.sin(heading_angle)
                    heading_vector = [heading_cos * scan_position_car[0] - heading_sin * scan_position_car[1], heading_sin * scan_position_car[0] + heading_cos * scan_position_car[1]]
                    
                    # shift to be centered at car position instead of origin
                    point_coord = [car_pos[0] + heading_vector[0], car_pos[1] + heading_vector[1]]

                    # get distance to lanes
                    x_on_lane1, y_on_lane1, _ = get_closest_point(point_coord, 1, 0.0)
                    x_on_lane2, y_on_lane2, _ = get_closest_point(point_coord, 2, 0.0)
                    point_on_lane1 = [x_on_lane1, y_on_lane1]
                    point_on_lane2 = [x_on_lane2, y_on_lane2]
                    distance_to_lane1 = self.compute_distance(point_on_lane1, point_coord)
                    distance_to_lane2 = self.compute_distance(point_on_lane2, point_coord)
                    
                    # update closest distances
                    if distance_to_lane1 < self.largest_distance_to_be_on_lane: # point is on lane 1
                        distance_to_car_on_lane = self.compute_distance(car_on_lane1, point_on_lane1)
                        # ^ lower bounds distance on circle
                        if distance_to_car_on_lane < smallest_distance_on_lane1:
                            smallest_distance_on_lane1 = distance_to_car_on_lane
                            
                    if distance_to_lane2 < self.largest_distance_to_be_on_lane: # point is on lane 2
                        distance_to_car_on_lane = self.compute_distance(car_on_lane2, point_on_lane2)
                        if distance_to_car_on_lane < smallest_distance_on_lane2:
                            smallest_distance_on_lane2 = distance_to_car_on_lane
            
            # get angle of next scan point        
            angle += data.angle_increment
            
        # publish distances
        self.distances_pub.publish(r = smallest_distance_on_lane1, g = smallest_distance_on_lane2)
        
if __name__ == '__main__':
    rospy.init_node('ass12_obstacle_detector')
    detector = ObstacleDetector()
    rospy.spin()
