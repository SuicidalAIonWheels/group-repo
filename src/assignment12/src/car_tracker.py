#!/usr/bin/env python

import numpy as np
import rospy
import math
from std_msgs.msg import ColorRGBA, UInt8
from nav_msgs.msg import Odometry
from tf.transformations import quaternion_matrix # quaternion to rotation matrix

from predict_closest_point import get_closest_point

class CarTracker:
    """
    Subscribes to the fake GPS and publishes data in two topics using the 
    colorRGBA message type because it offers tuple functionality:
    
    We adjust the car coordinates of the GPS to refer to the middle point 
    of the front axis and publish it together with the car's heading vector
    (which should be a unit vector):
        r = adjusted car x-coord
        g = adjusted car y-coord
        b = heading vector x-component
        a = heading vector y-component
    
    We also determine the closest point of the car on some oval track moved
    ahead by a certain distance and compute the angle between the car's heading
    vector and whatever vector points to that point from the adjusted car coordinates.
    Positive angle means track is on the right side of the car, 
    negative angle indicates track is on the left.
    We also look ahead some other distance to see if we'll be on a straight or a curve
    as feedback for the speed control.
    The data is published such that
        r = 1 if straight ahead, 0 if not
        g = /
        b = seconds since last published message
        a = angle
    b is used to determine the time difference for the D part of the drive PD controller.
    """
    def __init__(self):
        GPS_topic = "/localization/odom/8"                  # GPS odometry topic
        self.distance_front_axis_to_barcode = 0.26          # offset between barcode of car and front axis
        self.preview_distance = 0.7                         # how far to look ahead on the track
        self.preview_dist_for_speed = 0.0                  # how far to look ahead on the track to determine desired speed
        self.lane_ID = 1                                    # which lane we drive on
        self.timestamp_of_last_message = rospy.get_time()   # when the last angle_offset message has been published
        
        # publisher and subscriber
        self.GPS_sub = rospy.Subscriber(GPS_topic, Odometry, self.publish_car_data_from_GPS, queue_size=1)
        self.lane_ID_sub = rospy.Subscriber("drive_control/laneID", UInt8, self.update_lane_ID, queue_size=1)

        self.position_pub = rospy.Publisher("/car_tracker/position", ColorRGBA, queue_size=1)
        self.error_angle_pub = rospy.Publisher("/car_tracker/angle_offset", ColorRGBA, queue_size=1)

    def update_lane_ID(self, data):
        self.lane_ID = data.data
        
    def publish_car_data_from_GPS(self, data):
        """
        Positive angle means track is on the right side of the car, negative angle indicates track is on the left.
        """
        car_position = [data.pose.pose.position.x, data.pose.pose.position.y]
        lane_ID = self.lane_ID
        
        # get 2D heading vector
        quaternion = [data.pose.pose.orientation.x, data.pose.pose.orientation.y, data.pose.pose.orientation.z, data.pose.pose.orientation.w]
        hom_rotation_matrix = quaternion_matrix(quaternion)
        car_heading = np.dot(hom_rotation_matrix, [1,0,0,1])[0:2]
        
        # adjust car position from wherever the GPS sees the barcode to the middle point of the front axis
        car_position = [car_position[0] + self.distance_front_axis_to_barcode * car_heading[0], car_position[1] + self.distance_front_axis_to_barcode * car_heading[1]]
        
        # publish car position and heading (note that car heading should be a unit vector)
        self.position_pub.publish(r = car_position[0], g = car_position[1], b = car_heading[0], a = car_heading[1])
        
        # get point on oval and the vector pointing to it from the car
        point_on_track_x, point_on_track_y, _ = get_closest_point(car_position, lane_ID, self.preview_distance)
        direction_to_track = [point_on_track_x - car_position[0], point_on_track_y - car_position[1]]
        
        # angle of direction to the track relative to current heading
        angle = math.atan2(car_heading[1], car_heading[0]) - math.atan2(direction_to_track[1], direction_to_track[0])
        # need to clamp result between -pi and pi
        if angle > math.pi:
            angle -= 2.0 * math.pi
        elif angle < -math.pi:
            angle += 2.0 * math.pi
            
        # get section we will be on
        _, _, next_section_ID = get_closest_point(car_position, lane_ID, self.preview_dist_for_speed)
        set_r_of_msgs_to = 1.0
        if next_section_ID == 1 or next_section_ID == 3: # that is, curve
            set_r_of_msgs_to = 0.0
            
        # publish result
        last_timestamp = self.timestamp_of_last_message
        self.timestamp_of_last_message = rospy.get_time()
        self.error_angle_pub.publish(r = set_r_of_msgs_to, g = -1.0, b = self.timestamp_of_last_message - last_timestamp, a  = angle)	        

if __name__ == '__main__':
    rospy.init_node('ass12_car_tracker')
    tracker = CarTracker()
    rospy.spin()
