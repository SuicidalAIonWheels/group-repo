#!/usr/bin/env python

import numpy as np
import rospy
from std_msgs.msg import Int16, UInt8, ColorRGBA # want some form of tuple that can hold three floats

class DriveControl:
    """
    Steers the car according to angles derived from the camera "GPS".
    """
    def __init__(self):
        # data that needs to be kept track off
        self.current_angle_offset = 0                       # current angle error
        self.diff_current_angle_offset = 0                  # rate of change to previous error
        self.steering_angles = np.zeros(5)                  # remember the last few steering angles to smooth behaviour a bit
        self.current_lane = 1                               # current lane to drive on
        self.are_on_straight = True                         # whether we are on a straight or curve for speed setting
        
        # steering control parameter
        self.max_steering_angle_left = -0.7
        self.max_steering_angle_right = -self.max_steering_angle_left
        self.min_command_angle = 0
        self.max_command_angle = 179
        self.KP = 5.0 * 0.7 / 3.0
        self.KD = 0.01 * 0.7 / 3.0
        
        # other parameter
        self.min_obstacle_distance = 1.3                  # anything closer than that and we switch lanes
        self.is_blocked = False                             # ignore callbacks when an obstacle blocks both lanes
        self.speed_on_straight = 500                        # RPM to set on straights
        self.speed_on_curve = 475                           # RPM to set on curves
        
        # publisher and subscriber
        self.current_angle_sub = rospy.Subscriber("/car_tracker/angle_offset", ColorRGBA, self.update_current_car_data, queue_size=1)
        self.closest_obstacle_sub = rospy.Subscriber("/obstacle_detector", ColorRGBA, self.adjust_lane_selection, queue_size=1)
        
        self.speed_pub = rospy.Publisher("/manual_control/speed", Int16, queue_size=1)
        self.steering_pub = rospy.Publisher("/steering", UInt8, queue_size=1)
        self.lane_select_pub = rospy.Publisher("drive_control/laneID", UInt8, queue_size=1) # tells the tracker which lane to track
        
        # start driving with certain update frequency
        self.lane_select_pub.publish(1)
        rospy.sleep(1) # wait a second before starting to drive to give the trackers a bit of time to get going
        self.control_timer = rospy.Timer(rospy.Duration(1.0 / 100.0), self.drive) # 100 Hz
      
    def update_current_car_data(self, data):
        """
        Update data that is being tracked by the oval tracker.
        """
        if not self.is_blocked: # ignore callbacks if we encountered a blocking obstacle
            # if we are on a straight or not
            self.are_on_straight = (data.r == 1.0)
            
            # angle
            previous_offset = self.current_angle_offset
            self.current_angle_offset = data.a
            
            # compute rate of change (data.b holds time difference)
            if data.b > 0:
                self.diff_current_angle_offset = (self.current_angle_offset - previous_offset) / data.b
        
    def adjust_lane_selection(self, data):
        """
        Switch lanes or stop based on obstacles ahead.
        """
        if not self.is_blocked: # ignore callbacks if we encountered a blocking obstacle
            # if both lanes are blocked shut down
            if data.r < self.min_obstacle_distance and data.g < self.min_obstacle_distance:
                print("Cannot drive around obstacle!")
                self.control_timer.shutdown()
                self.speed_pub.publish(0)
                self.is_blocked = True
            
            # otherwise check if we have to switch
            if self.current_lane == 1 and data.r < self.min_obstacle_distance:
                self.lane_select_pub.publish(2)
                self.current_lane = 2
            elif self.current_lane == 2 and data.g < self.min_obstacle_distance:
                self.lane_select_pub.publish(1)
                self.current_lane = 1
    
    def drive(self, event): # timer passes timestamp which we don't need
        """
        Adjusts steering.
        """
        if not self.is_blocked: # ignore if we encountered a blocking obstacle and timer.shutdown() doesn't work for some reason
            
            # determine the desired steering angle and clamp it between its extrema
            desired_steering_angle = self.KP * self.current_angle_offset - self.KD * self.diff_current_angle_offset
            
            if desired_steering_angle < self.max_steering_angle_left:
                desired_steering_angle = self.max_steering_angle_left
            elif desired_steering_angle > self.max_steering_angle_right:
                desired_steering_angle = self.max_steering_angle_right
            
            # attempt to smooth the angle out a bit to reduce jittering    
            self.steering_angles = np.append(self.steering_angles[1:], [desired_steering_angle])
            smoothed_desired_angle = np.average(self.steering_angles)
            
            # publish command angle; use a simple linear transformation
            enumerator = (smoothed_desired_angle - self.max_steering_angle_left) * (self.max_command_angle - self.min_command_angle)
            denominator = self.max_steering_angle_right - self.max_steering_angle_left
            desired_command_angle = int(round(enumerator / denominator + self.min_command_angle))
            
            # determine desired speed
            desired_speed = self.speed_on_curve
            if self.are_on_straight:
                desired_speed = self.speed_on_straight
            
            # publish
            self.steering_pub.publish(desired_command_angle)
            self.speed_pub.publish(desired_speed)
        
if __name__ == '__main__':
    rospy.init_node('ass12_drive_control')
    drive_control = DriveControl()
    rospy.spin()
