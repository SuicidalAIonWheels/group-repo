#!/usr/bin/env python

import numpy as np
import rospy
import os # to get location of this file
from std_msgs.msg import Int16, UInt8, ColorRGBA # want some form of tuple that can hold three floats
from datetime import datetime

class LogCoords:
    """
    Logs car coordinate as long as it runs and saves them to a npy file upon termination.
    """
    def __init__(self):
        self.car_coordinates = []
        
        self.position_sub = rospy.Subscriber("/car_tracker/position", ColorRGBA, self.log_coordinate, queue_size=1)
      
    def log_coordinate(self, data):
        self.car_coordinates.append([data.r, data.g])
        
    def write_to_file(self):
        filepath = os.path.dirname(os.path.abspath(__file__)) + "/location_log_" + datetime.now().strftime("%Y_%m_%d__%H_%M_%S") + ".npy"
        np.save(filepath, np.array(self.car_coordinates))
        print("Logged car positions at " + filepath)
        
if __name__ == '__main__':
    rospy.init_node('ass12_coordinate_logger')
    logger = LogCoords()
    rospy.spin()
    
    rospy.on_shutdown(logger.write_to_file)
