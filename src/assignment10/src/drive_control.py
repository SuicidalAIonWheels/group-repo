#!/usr/bin/env python

import numpy as np
import rospy
import os # to get location of this file
from std_msgs.msg import Int16, UInt8, ColorRGBA # want some form of tuple that can hold three floats
from datetime import datetime

class DriveControl:
    """
    Steers the car according to angles derived from the camera "GPS".
    """
    def __init__(self):
        self.current_angle_sub = rospy.Subscriber("/oval_tracker/angle_and_pos", ColorRGBA, self.update_current_car_data, queue_size=1)
        
        self.speed_pub = rospy.Publisher("/manual_control/speed", Int16, queue_size=1)
        self.steering_pub = rospy.Publisher("/steering", UInt8, queue_size=1)
        
        self.current_angle = 0
        self.previous_angle = 0
        # so that we can log the current position
        self.car_coordinates = []
        
        # steering control parameter
        self.max_steering_angle_left = -0.7
        self.max_steering_angle_right = -self.max_steering_angle_left
        self.min_command_angle = 0
        self.max_command_angle = 179
        
        # controller weights
        self.KP = 14.0 * 0.7 / 3.0
        self.KD = 0.1 * 0.7 / 3.0
        
        # frequency in Hz with which to adjust the steering
        self.frequency = 20.0
        self.sleep_time = 1.0 / self.frequency
      
    def update_current_car_data(self, data):
        """
        Keep known angle up-to-date and log position.
        """
        self.current_angle = data.a
        self.car_coordinates.append([data.r, data.g])
        
    def log_coordinates(self):
        """
        Saves the car coordinates to a .npy file so that we can retrieve the data later for plotting.
        """
        filepath = os.path.dirname(os.path.abspath(__file__)) + "/location_log_" + datetime.now().strftime("%Y_%m_%d__%H_%M_%S") + ".npy"
        np.save(filepath, np.array(self.car_coordinates))
        print("Logged car positions at " + filepath)
    
    def drive(self):
        """
        Drives the car along the oval.
        """
        while not rospy.is_shutdown():
            self.speed_pub.publish(250) # we'll drive with constant speed
            angle = self.current_angle # lock current angle
            diff_angle = (angle - self.previous_angle) * self.frequency # rate of change
            
            # determine the desired steering angle and clamp it between its extrema
            desired_steering_angle = self.KP * angle - self.KD * diff_angle
            
            if desired_steering_angle < self.max_steering_angle_left:
                desired_steering_angle = self.max_steering_angle_left
            elif desired_steering_angle > self.max_steering_angle_right:
                desired_steering_angle = self.max_steering_angle_right
            
            # publish command angle; use a simple linear transformation
            enumerator = (desired_steering_angle - self.max_steering_angle_left) * (self.max_command_angle - self.min_command_angle)
            denominator = self.max_steering_angle_right - self.max_steering_angle_left
            desired_command_angle = int(round(enumerator / denominator + self.min_command_angle))
            
            self.steering_pub.publish(desired_command_angle)
            
            self.previous_angle = angle
            rospy.sleep(self.sleep_time)
        
if __name__ == '__main__':
    rospy.init_node('ass10_drive_control')
    drive_control = DriveControl()
    drive_control.drive()
    rospy.on_shutdown(drive_control.log_coordinates)
