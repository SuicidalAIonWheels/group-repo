#!/usr/bin/env python
import numpy as np
import math

def get_closest_point(point, laneID, distance):
    """
    Given some point (x,y), computes the closest point on the oval track
    from exercise 9.
    """
    if distance < 0.0:
        print("distance must not be negative!")
        return None

    # we required a shift of about 5 cm from the image coordinates to the real ones
    shift = 0.05 
    # define parameters of the oval
    if laneID == 1: # inner oval
        left_line_y = 0.83 + shift
        right_line_y = 3.52 - shift
    elif laneID == 2: # outer oval
        left_line_y = 0.51 + shift
        right_line_y = 3.84 - shift
    else:
        print("Unknown laneID {}!".format(laneID))
        return None

    lines_min_x = 1.87
    lines_max_x = 4.14
    circle_center_y = (left_line_y + right_line_y) / 2.0
    circle_radius = circle_center_y - left_line_y
    max_distance_on_circle = circle_radius * math.pi # halved circumference
    
    """
    Find the actual closest point on the oval first:
    We can figure out based on the x coordinate which part of the track
    is closest: If x < lines_min_x then the closest point is on the
    upper half circle, if x > lines_max_x it is on the lower one.
    Otherwise x lies within [lines_min_x, lines_max_x] and the closest
    point is on one of the straight line segments. Note that the mid
    point of the circles, which would have infinitely many solutions,
    is taken care of by this.
    
    We further remember the location of the point:
        0 - left straight
        1 - lower circle
        2 - right straight
        3 - upper circle
    """
    if point[0] < lines_min_x:
        # The point we want is the intersection of the line through
        # the circle center C and P=(x,y) with the circle itself.
        # This is given by C + circle_radius * (P-C) / norm(P-C)
        normPC = math.sqrt((point[0] - lines_min_x)**2 + (point[1] - circle_center_y)**2)
        
        closest_x = lines_min_x + circle_radius * (point[0] - lines_min_x) / normPC
        closest_y = circle_center_y + circle_radius * (point[1] - circle_center_y) / normPC
        locationID = 3
    elif point[0] > lines_max_x:
        normPC = math.sqrt((point[0] - lines_max_x)**2 + (point[1] - circle_center_y)**2)
        
        closest_x = lines_max_x + circle_radius * (point[0] - lines_max_x) / normPC
        closest_y = circle_center_y + circle_radius * (point[1] - circle_center_y) / normPC
        locationID = 1
    else:
        distance_left = abs(point[1] - left_line_y)
        distance_right = abs(point[1] - right_line_y)
        if distance_left < distance_right:
            closest_x = point[0]
            closest_y = left_line_y
            locationID = 0
        else:
            closest_x = point[0]
            closest_y = right_line_y
            locationID = 2
            
    # now offset the point by the desired distance along the track
    # requires that the distance is positive and moves in counterclockwise direction
    return_point = [closest_x, closest_y]
    remaining_distance = distance
    while remaining_distance > 0.0:
        if locationID == 0:
            # we are on the left straight, so offset x until we reach the end
            return_point[0] += remaining_distance
            if return_point[0] > lines_max_x: # we moved onto the lower circle
                remaining_distance = return_point[0] - lines_max_x
                return_point[0] = lines_max_x
                locationID = 1
            else: # no distance remaining
                remaining_distance = 0.0
            
        elif locationID == 1:
            # We are on the lower circle at some angle we can compute using atan2. 
            # We can rotate until we enter the right straight at an angle of pi/2.
            # A distance of 1 meter corresponds to an angle of 1/radius.
            current_angle = math.atan2(return_point[1] - circle_center_y, return_point[0] - lines_max_x)
            max_angle = math.pi / 2.0 - current_angle
            angle_of_remaining_distance = remaining_distance / circle_radius
            if angle_of_remaining_distance > max_angle:
                # we move to the start of the right straight
                return_point[0] = lines_max_x
                return_point[1] = right_line_y
                locationID = 2
                # remaining distance from remaining angle
                remaining_distance = (angle_of_remaining_distance - max_angle) * circle_radius
            else:
                # get new point based on circle center and the new angle
                new_angle = current_angle + angle_of_remaining_distance
                return_point[0] = lines_max_x + math.cos(new_angle) * circle_radius
                return_point[1] = circle_center_y + math.sin(new_angle) * circle_radius
                remaining_distance = 0.0
            
        elif locationID == 2:
            # we are on the right straight, so move up
            return_point[0] -= remaining_distance
            if return_point[0] < lines_min_x: # moved onto upper circle
                remaining_distance = lines_min_x - return_point[0]
                return_point[0] = lines_min_x
                locationID = 3
            else:
                remaining_distance = 0.0
            
        elif locationID == 3:
            # We are on the upper circle at some angle we can compute using atan2. 
            # However, atan2 will now output from pi/2 to pi for the right quadrant in counterclockwise
            # direction and then from -pi to -pi/2 on the left quadrant. We handle this by
            # shifting negative angles by 2pi. We enter the left straight at 3pi/2.
            current_angle = math.atan2(return_point[1] - circle_center_y, return_point[0] - lines_min_x)
            if current_angle < 0.0:
                current_angle += 2.0 * math.pi
            max_angle = 3.0 * math.pi / 2.0 - current_angle
            angle_of_remaining_distance = remaining_distance / circle_radius
            if angle_of_remaining_distance > max_angle:
                # we move to the start of the left straight
                return_point[0] = lines_min_x
                return_point[1] = left_line_y
                locationID = 0
                # remaining distance from remaining angle
                remaining_distance = (angle_of_remaining_distance - max_angle) * circle_radius
            else:
                # get new point based on circle center and the new angle
                new_angle = current_angle + angle_of_remaining_distance
                return_point[0] = lines_min_x + math.cos(new_angle) * circle_radius
                return_point[1] = circle_center_y + math.sin(new_angle) * circle_radius
                remaining_distance = 0.0
            
        else:
            print("Encountered unknown location ID!")
            return None
        
    return return_point
            
if __name__ == '__main__':
    print(get_closest_point([3,2], 1, 0.5))
    print(get_closest_point([1,1], 2, 0.2))

