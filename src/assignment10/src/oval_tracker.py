#!/usr/bin/env python

import numpy as np
import rospy
import math
from std_msgs.msg import ColorRGBA # want some form of tuple that can hold three floats
from nav_msgs.msg import Odometry
from tf.transformations import quaternion_matrix # quaternion to rotation matrix

from predict_closest_point import get_closest_point

class OvalTracker:
    """
    Tracks the car's position relative to the oval track. Subscribes to the "GPS" camera's
    odometry topic and publishes the current angle between the car's heading vector and
    the vector that points to where we want to be on the track in the future.
    """
    def __init__(self):
        GPS_topic = "/localization/odom/11" # GPS odometry topic
        self.preview_distance = 0.5 # how far to look ahead on the track
        self.laneID = 1 # which lane we drive on (for example if the other line is occupied)
        
        self.GPS_sub = rospy.Subscriber(GPS_topic, Odometry, self.determine_angle_from_GPS, queue_size=1)
        # publishes such that r = car x-coord, g = car y-coord, a = angle
        self.angle_and_pos_pub = rospy.Publisher("/oval_tracker/angle_and_pos", ColorRGBA, queue_size=1)
        
    def determine_angle_from_GPS(self, data):
        """
        Positive angle means track is on the right side of the care, negative angle indicates track is on the left.
        """
        car_position = [data.pose.pose.position.x, data.pose.pose.position.y]
        
        # get 2D heading vector
        quaternion = [data.pose.pose.orientation.x, data.pose.pose.orientation.y, data.pose.pose.orientation.z, data.pose.pose.orientation.w]
        hom_rotation_matrix = quaternion_matrix(quaternion)
        car_heading = np.dot(hom_rotation_matrix, [1,0,0,1])[0:2]
        
        # get point on oval and direction to it
        point_on_track = get_closest_point(car_position, self.laneID, self.preview_distance)
        direction_to_track = [point_on_track[0] - car_position[0], point_on_track[1] - car_position[1]]
        
        # angle of direction to the track relative to current heading
        angle = math.atan2(car_heading[1], car_heading[0]) - math.atan2(direction_to_track[1], direction_to_track[0])
        # need to clamp result between -pi and pi
        if angle > math.pi:
            angle -= 2.0 * math.pi
        elif angle < -math.pi:
            angle += 2.0 * math.pi
            
        # publish result
        self.angle_and_pos_pub.publish(r = car_position[0], g = car_position[1], a = angle)
        
if __name__ == '__main__':
    rospy.init_node('ass10_oval_tracker')
    tracker = OvalTracker()
    rospy.spin()
