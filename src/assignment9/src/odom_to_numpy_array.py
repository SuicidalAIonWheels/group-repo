#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
import numpy as np

class OdomPosToNumpyArray:
    """
    Listens to the odometry position, converts the x and y coordinates to a numpy array of points
    and saves everything as a .npy binary file.
    """
    def __init__(self):
        self.odometry_sub = rospy.Subscriber("/localization/odom/9", Odometry, self.callback, queue_size=1)
        self.positions = []
        
    def callback(self, data):
        xcoord = data.pose.pose.position.x
        ycoord = data.pose.pose.position.y
        
        self.positions.append([xcoord, ycoord])
        
    def save_array(self):
        np.save("path_to_file", np.array(self.positions))
        print("saved array")
            
if __name__ == '__main__':
    rospy.init_node('ass9_odom_listener')
    listener = OdomPosToNumpyArray()
    rospy.spin()
    rospy.on_shutdown(listener.save_array)
