#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from std_msgs.msg import Float32

# essentially re-prints the data
def callback(raw_msg):
    publisher.publish("I heard: " + str(raw_msg))

# initialize the node
rospy.init_node("assignment1_publisher_subscriber_node")

# create publisher
publisher = rospy.Publisher("/assignment1_publisher_subscriber", String, queue_size=10)#

# create subscriber
rospy.Subscriber("/yaw", Float32, callback)

# keep waiting for further /yaw output
rospy.spin()

