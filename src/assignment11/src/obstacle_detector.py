#!/usr/bin/env python

import numpy as np
import rospy
import math
from std_msgs.msg import ColorRGBA 
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan
from tf.transformations import quaternion_matrix # quaternion to rotation matrix

from predict_closest_point import get_closest_point

class ObstacleDetector:
    """
    Detects obstacles on both lanes. Uses ColorRGBA to output the distance to the closest obstacle
    on lane 1 at "r" and to lane 2 at "g".
    """
    def __init__(self):
        # parameter
        GPS_topic = "/localization/odom/6"         # GPS odometry topic
        self.largest_distance_to_be_on_lane = 0.05 # how far away a point can be from the lane centre to be considered on it
        
        # track car position and orientation
        self.current_x = 0.0
        self.current_y = 0.0
        self.current_heading_rotation_matrix = np.zeros((4,4))
        
        # publisher and subscriber
        self.GPS_sub = rospy.Subscriber(GPS_topic, Odometry, self.get_car_data_from_GPS, queue_size=1)
        self.lazor_sub = rospy.Subscriber("/scan", LaserScan, self.publish_nearest_obstacle_distances, queue_size=1)
        
        self.distance_pub = rospy.Publisher("/obstacle_detector", ColorRGBA, queue_size=1)
        
    def get_car_data_from_GPS(self, data):
        """
        Update current position and rotation matrix.
        """
        self.current_x = data.pose.pose.position.x
        self.current_y = data.pose.pose.position.y
        
        quaternion = [data.pose.pose.orientation.x, data.pose.pose.orientation.y, data.pose.pose.orientation.z, data.pose.pose.orientation.w]
        self.current_heading_rotation_matrix = quaternion_matrix(quaternion)
        
    def compute_distance(self, point1, point2):
        return  math.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)
        
    def publish_nearest_obstacle_distances(self, data):
        """
        Computes and publishes distance to closest obstacle on lane 1 and 2.
        """
        smallest_distance_on_lane1 = float("inf")
        smallest_distance_on_lane2 = float("inf")
        
        # lock current position and rotation
        car_x = self.current_x
        car_y = self.current_y
        car_on_lane1 = get_closest_point([car_x, car_y], 1, 0.0)
        car_on_lane2 = get_closest_point([car_x, car_y], 2, 0.0)
        rotation_matrix = np.copy(self.current_heading_rotation_matrix)
        
        # consider every data point
        distances = np.asarray(data.ranges)
        angle = data.angle_min # angle of current point
        # The angle of zero is at the back of the car, +- pi is at the front, -pi/2 is the left, pi/2 the right side.
        # The heading vector of the respective scan relative to the car is therefore (cos(angle + pi), sin(angle + pi)).
        # We also only consider points ahead of the car, so angles with absolute value above 3pi/4.
        angle_cutoff = 3.0 * math.pi / 4.0 # smallest absolute angle to consider
        for distance in distances:
            if data.range_min < distance and distance < data.range_max: # documentation says to discard points outside of this
                if abs(angle) >= angle_cutoff:
                    # compute global coordinates of the current data point
                    heading_vector = np.dot(rotation_matrix, [math.cos(angle + math.pi), math.sin(angle + math.pi), 0, 1])[0:2]
                    point_coord = [car_x, car_y] + distance * heading_vector
                    
                    # get distance to lanes
                    point_on_lane1 = get_closest_point(point_coord, 1, 0.0)
                    point_on_lane2 = get_closest_point(point_coord, 2, 0.0)
                    distance_to_lane1 = self.compute_distance(point_on_lane1, point_coord)
                    distance_to_lane2 = self.compute_distance(point_on_lane2, point_coord)
                    
                    # update closest distances
                    if distance_to_lane1 < self.largest_distance_to_be_on_lane:
                        distance_to_car_on_lane = self.compute_distance(car_on_lane1, point_on_lane1)
                        # ^ lower bounds distance on circle
                        if distance_to_car_on_lane < smallest_distance_on_lane1:
                            smallest_distance_on_lane1 = distance_to_car_on_lane
                            
                    if distance_to_lane2 < self.largest_distance_to_be_on_lane:
                        distance_to_car_on_lane = self.compute_distance(car_on_lane2, point_on_lane2)
                        if distance_to_car_on_lane < smallest_distance_on_lane2:
                            smallest_distance_on_lane2 = distance_to_car_on_lane
                    
            angle += data.angle_increment
            
        # publish distances
        self.distance_pub.publish(r = smallest_distance_on_lane1, g = smallest_distance_on_lane2)
        
if __name__ == '__main__':
    rospy.init_node('ass11_obstacle_detector')
    detector = ObstacleDetector()
    rospy.spin()
