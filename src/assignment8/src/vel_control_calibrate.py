#!/usr/bin/env python

import rospy
from nav_msgs.msg import Odometry
from std_msgs.msg import Int16, Float32
import numpy as np

class VelConCalib:
    def __init__(self):
        self.current_velocity_sub = rospy.Subscriber("/odom", Odometry, self.get_current_velocity, queue_size=1)
        self.pub_velocity = rospy.Publisher("/velocity", Float32, queue_size=1)
    
    def get_current_velocity(self, data):
        """
        Publishes odometry velocity separately (so it can be plotted).
        """
        current_velocity = float(data.twist.twist.linear.x)
        self.pub_velocity.publish(current_velocity)
        
if __name__ == '__main__':
    rospy.init_node('ass8_velcon_calibrate')
    controller = VelConCalib()
    rospy.spin()
