#!/usr/bin/env python

import rospy
from std_msgs.msg import Int16, Float32, UInt8
import numpy as np

class DriveControl:
    def __init__(self):
        self.current_offset_sub = rospy.Subscriber("/line_tracker/offset", Int16, self.update_current_offset, queue_size=1)
        
        #self.desired_speed_pub = rospy.Publisher("/desired_speed", Float32, queue_size=1)
        self.desired_speed_pub = rospy.Publisher("/manual_control/speed", Int16, queue_size=1)
        self.steering_pub = rospy.Publisher("/steering", UInt8, queue_size=1)
        
        self.current_offset = 0
        self.previous_offset = 0
        
        # steering control parameter from previous exercise
        self.max_steering_angle_left = -0.7
        self.max_steering_angle_right = -self.max_steering_angle_left
        self.min_command_angle = 0
        self.max_command_angle = 179
        self.KP = 0.7 * 0.7 / 400.0
        self.KD = 0.1 * 0.7 / 400.0
      
    def update_current_offset(self, data):
        self.current_offset = int(data.data)
    
    def drive(self):
        """
        Drives the car following a white line on the track.
        """
        while not rospy.is_shutdown():
            self.desired_speed_pub.publish(150) # we'll just drive with constant speed for now
            x_offset = self.current_offset # lock current offset
            Diff_offset = x_offset - self.previous_offset * 10 # rate of offset change
            
            desired_steering_angle = self.KP * x_offset - self.KD * Diff_offset
            
            # publish command angle; we use a simple linear transformation
            enumerator = (desired_steering_angle - self.max_steering_angle_left) * (self.max_command_angle - self.min_command_angle)
            denominator = self.max_steering_angle_right - self.max_steering_angle_left
            desired_command_angle = enumerator / denominator + self.max_steering_angle_left
            if desired_command_angle < 0:
                desired_command_angle = 0
            elif desired_command_angle > 179:
                desired_command_angle = 179
            
            print(desired_command_angle)
            self.steering_pub.publish(desired_command_angle)
            
            self.previous_offset = x_offset
            rospy.sleep(0.1)
        
if __name__ == '__main__':
    rospy.init_node('ass8_drive_control')
    drive_control = DriveControl()
    drive_control.drive()
