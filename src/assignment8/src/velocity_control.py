#!/usr/bin/env python

import rospy
from nav_msgs.msg import Odometry
from std_msgs.msg import Int16, Float32
import numpy as np

class VelocityController:
    def __init__(self):
        self.current_velocity = 0.0
        self.desired_velocity = 0.0
        self.last_speed_command = 0
        self.desired_speed_sub = rospy.Subscriber("/desired_speed", Float32, self.get_desired_speed, queue_size=1) # keeps track of currently desired speed
        self.current_velocity_sub = rospy.Subscriber("/odom", Odometry, self.get_current_velocity, queue_size=1) # keeps track of current vehicle speed
        self.pub_speed = rospy.Publisher("/manual_control/speed", Int16, queue_size=1)
        
        # controller parameter
        self.K = 0.6 * 0.2 * 150.0 * 0.35
        self.Ti = 0.5 * 1.0 * 150.0 * 0.05
        self.Td = 0.125 * 1.0 * 150.0 * 0.05

    def get_desired_speed(self, data):
        """
        Reads in the currently desired speed.
        """
        self.desired_velocity = float(data.data)
    
    def get_current_velocity(self, data):
        """
        Exracts the current velocity from the odometry topic.
        """
        self.current_velocity = float(data.twist.twist.linear.x)
        
    def control_speed(self):
        """
        Runs until termination controlling the speed.
        """
        previous_errors = np.zeros(10)
        while not rospy.is_shutdown():
            current_speed = self.current_velocity
            desired_speed = self.desired_velocity # get lock for these since they may be altered
            
            # compute parts of the PID control signal
            current_error = desired_speed - current_speed
            previous_errors = np.append(previous_errors[1:], [current_error])
            integral_part = np.sum(previous_errors) * 0.1 # every velocity has been maintained for about 0.1 seconds
            diff_part = (previous_errors[-1] - previous_errors[-2]) * 10
            
            # get control signal and publish
            adjust_speed_rpm = int(self.K * (current_error + integral_part / self.Ti + diff_part * self.Td))
            self.last_speed_command = self.last_speed_command + adjust_speed_rpm
            self.pub_speed.publish(self.last_speed_command)
            
            rospy.sleep(0.1)
            
        self.pub_speed.publish(0)
        
if __name__ == '__main__':
    rospy.init_node('ass8_velocity_control')
    controller = VelocityController()
    controller.control_speed()
