#!/usr/bin/env python

import rospy
import cv2
from std_msgs.msg import Int16, Float32
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np

class LineTracker:
    def __init__(self):
        self.camera_sub = rospy.Subscriber("/camera/color/image_raw", Image, self.get_line_offset, queue_size=1)
        self.blackwhite_image_pub = rospy.Publisher("/line_tracker/blackwhite", Image, queue_size = 1) # publish black_white image
        self.offset_pub = rospy.Publisher("/line_tracker/offset", Int16, queue_size=1) # publish current offset from image center
        self.bridge = CvBridge()
        
        self.last_offset = 0 # remember last offset so we will not get distracted by additional lines in our field of view
        
    def get_line_offset(self, image):
        """
        Determines the current offset of the white line and publishes it.
        """
        try:
            cv_image = self.bridge.imgmsg_to_cv2(image, "bgr8")
        except CvBridgeError as e:
            print(e)
            
        x_image_center = int(cv_image.shape[1] / 2)
        y_image_bottom = cv_image.shape[0] - 1 # largest y-coordinate

        # make it gray
        gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        
        # turn black/white
        bi_gray_max = 255
        bi_gray_min = 200
        ret, thresh1 = cv2.threshold(gray, bi_gray_min, bi_gray_max, cv2.THRESH_BINARY);
        
        try:
            self.blackwhite_image_pub.publish(self.bridge.cv2_to_imgmsg(thresh1, "mono8"))
        except CvBridgeError as e:
            print(e)
        
        # we look for the white point with an offset closest to the last one within the bottom 50 pixels of the image
        best_current_offset = x_image_center
        if self.last_offset < 0:
            best_current_offset = -x_image_center
        y = y_image_bottom - 50
        while y > y_image_bottom - 100:
            for x in range(thresh1.shape[1]):
                if thresh1[y][x] > 0: # white pixel
                    current_offset = x - x_image_center
                    if abs(current_offset - self.last_offset) < abs(best_current_offset - self.last_offset):
                        # offset of this white pixel is more similar to the previous offset than the current best known
                        best_current_offset = current_offset
            
            y = y - 1
            
        self.offset_pub.publish(best_current_offset)
        
if __name__ == '__main__':
    rospy.init_node('ass8_line_tracker')
    tracker = LineTracker()
    rospy.spin()
