#!/usr/bin/env python
import roslib
import sys
import rospy
import cv2
import numpy as np
import math
from std_msgs.msg import String
from std_msgs.msg import Float32
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class line_detector:

    def __init__(self):
        self.image_pub = rospy.Publisher("/line_detector/lines_drawn", Image, queue_size = 1)
        self.blackwhite_image_pub = rospy.Publisher("/line_detector/blackwhite", Image, queue_size = 1)
        self.leftLine_m_pub = rospy.Publisher("/line_detector/left_line_m", Float32, queue_size = 1)
        self.leftLine_b_pub = rospy.Publisher("/line_detector/left_line_b", Float32, queue_size = 1)
        self.rightLine_m_pub = rospy.Publisher("/line_detector/right_line_m", Float32, queue_size = 1)
        self.rightLine_b_pub = rospy.Publisher("/line_detector/right_line_b", Float32, queue_size = 1)

        self.bridge = CvBridge()
        # adjust to name of image publisher as needed
        # start image publisher via rosrun image_publisher image_publisher path-to-png
        self.image_sub = rospy.Subscriber("/image_publisher_1543418881551437951/image_raw", Image, self.callback, queue_size=1)

    # Fits a line to the given points in an image using RANSAC.
    # Note that it assumes that the x-axis goes from left to right, y from bottom to top.
    # Just like CV2's fitline() function, returns a normal vector and a point on the line
    # (since it applies fitline() to the determined selection)
    def RANSAClinefit(self, points, maxNumIter, maxDistance, requiredInlierPercentage):
        iteration = 0
        fitThesePoints = None
        d = requiredInlierPercentage * points.shape[0]
        
        while iteration < maxNumIter:
            firstPointIndex = np.random.randint(0, points.shape[0])
            secondPointIndex = np.random.randint(0, points.shape[0])
                
            firstPoint = points[firstPointIndex, :]
            secondPoint = points[secondPointIndex, :]
            
            # fit line to the two selected points
            if firstPoint[0] != secondPoint[0]: # ignore bad pair
                m = (firstPoint[1] - secondPoint[1]) / float(firstPoint[0] - secondPoint[0])
                b = firstPoint[1] - m * firstPoint[0]
            
                # determine if we have enough inliers
                inliers = []
                for i in range(points.shape[0]):
                    candidate = points[i,:]
                    distance = abs(m * candidate[0] - candidate[1] + b) / math.sqrt(m * m + 1)
                    if distance < maxDistance:
                        inliers.append(candidate)
                
                if len(inliers) >= d:
                    fitThesePoints = np.array(inliers)
                    break
                
        if fitThesePoints is None:
            raise Exception('Unable to find good fit!')
            
        return cv2.fitLine(fitThesePoints, cv2.DIST_L2, 0, 0.01, 0.01)

    def callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        # make it gray
        gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        
        # turn black/white
        bi_gray_max = 255
        bi_gray_min = 240
        ret, thresh1 = cv2.threshold(gray, bi_gray_min, bi_gray_max, cv2.THRESH_BINARY);
        
        try:
            self.blackwhite_image_pub.publish(self.bridge.cv2_to_imgmsg(thresh1, "mono8"))
        except CvBridgeError as e:
            print(e)
        
        # we scan the image horizontally looking for white boundary points
        leftLineList = []
        rightLineList = []
        for y in range(thresh1.shape[0]):
            x = 0
            leftLineLeftBoundaryX = None
            leftLineRightBoundaryX = None
            rightLineLeftBoundaryX = None
            rightLineRightBoundaryX = None
            
            # look for left boundary of left line
            while x < thresh1.shape[1]:
                if thresh1[y][x] > 0:
                    leftLineLeftBoundaryX = x
                    break
                x += 1
            
            if leftLineLeftBoundaryX is not None:
                # look for right boundary of left line
                while x < thresh1.shape[1]:
                    if thresh1[y][x] == 0:
                        leftLineRightBoundaryX = x - 1
                        break
                    x += 1
                
                if leftLineRightBoundaryX is not None:
                    # look for left boundary of right line
                    while x < thresh1.shape[1]:
                        if thresh1[y][x] > 0:
                            rightLineLeftBoundaryX = x
                            break
                        x += 1
                        
                    if rightLineLeftBoundaryX is not None:
                        # look for right boundary of right line
                        while x < thresh1.shape[1]:
                            if thresh1[y][x] == 0:
                                rightLineRightBoundaryX = x - 1
                                break
                            x += 1
                            
            # we ignore any line where we have not found all four values
            if rightLineRightBoundaryX is not None:
                leftLineList.append([(leftLineLeftBoundaryX + leftLineRightBoundaryX) / float(2), y])
                rightLineList.append([(rightLineLeftBoundaryX + rightLineRightBoundaryX) / float(2), y])

        leftLinePoints = np.array(leftLineList)
        rightLinePoints = np.array(rightLineList)

        # fit line
        RANSACleft = self.RANSAClinefit(leftLinePoints, 500, 10, 0.9) # determined parameter by trial and error
        RANSACright = self.RANSAClinefit(rightLinePoints, 500, 10, 0.9)

        # We now have a point on the line and a normal vector N. Algebraically, the slope is - N_x / N_y,
        # however the actual y-axis of the image points downwards, so the normal vector turns out to be the
        # tangent vector within image coordinates. Thus, m = N_y / N_x.
        leftM = RANSACleft[1,0] / RANSACleft[0,0]
        leftB = RANSACleft[3,0] - leftM * RANSACleft[2,0]
        rightM = RANSACright[1,0] / RANSACright[0,0]
        rightB = RANSACright[3,0] - rightM * RANSACright[2,0]

        # draw lines (cv2.line() doesn't care whether the provided points actually are in the image)
        cv2.line(cv_image, (0, int(leftB)), (cv_image.shape[1] - 1, int(leftM * (cv_image.shape[1] - 1) + leftB)), (0,255,0), 3)
        cv2.line(cv_image, (0, int(rightB)), (cv_image.shape[1] - 1, int(rightM * (cv_image.shape[1] - 1) + rightB)), (255,0,0), 3)
        
        try:
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
        except CvBridgeError as e:
            print(e)
          
        self.leftLine_m_pub.publish(leftM)
        self.leftLine_b_pub.publish(leftB)
        self.rightLine_m_pub.publish(rightM)
        self.rightLine_b_pub.publish(rightB)
      

def main(args):
  rospy.init_node('line_detector', anonymous=True)
  detector = line_detector()
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
