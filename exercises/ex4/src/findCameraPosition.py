#!/usr/bin/env python

import sys
import cv2
import numpy as np
import math

# find points
image = cv2.imread('../img/blackwhite_image.png')
pointIsCloseBound = 50
img_points = np.zeros((6,2,1))
numberOfPointsFound = 0
# shape[0] is apparently height, shape[1] width
for y in range(image.shape[0]):
    for x in range(image.shape[1]):
        # every pixel is either (0,0,0) black or (255,255,255) white
        if image[y][x][0] > 0:
            discardPoint = False # we only keep the first coordinate pair for every white point
            if numberOfPointsFound > 0: # we add the very first coordinate no matter what
                # only add this new coordinate if it is not close to the points we have already found
                for i in range(img_points.shape[0]):
                    currentCoord = img_points[i,:,0]
                    if abs(currentCoord[0] - x) < pointIsCloseBound and abs(currentCoord[1] - y) < pointIsCloseBound:
                        discardPoint = True
                        break
                
            if not discardPoint:
                img_points[numberOfPointsFound,:,0] = np.array([x, y])
                numberOfPointsFound += 1

# define camera matrix and distortion matrix
fx = 614.1699
fy = 614.9002
#cx = 329.9491
#cy = 237.2788
cx = image.shape[1] / float(2)
cy = image.shape[0] / float(2) # principal point should be at the image center
# the other parameters fx, fy, k1, k2, p1 and p2 apparently don't quite fit the vehicle camera either
camera_mat = np.zeros((3,3,1))
camera_mat[:,:,0] = np.array([[fx, 0, cx],
                        [0, fy, cy],
                        [0, 0, 1]])
                        
k1 = 0.1115
k2 = -0.1089
p1 = 0
p2 = 0
dist_coeffs = np.zeros((4,1))
dist_coeffs[:,0] = np.array([[k1, k2, p1, p2]])

# find rotation matrix and translation vector while printing results
print("image coordinates:")
print(img_points[:,:,0])

obj_points = np.zeros((6,3,1))
# based on the image, the points will be found from far to close, right to left
obj_points[:,:,0] = np.array([[40.0,59.0,0.0],
                            [0.0,59.0,0.0],
                            [40.0,28.0,0.0],
                            [0.0,28.0,0.0],
                            [40.0,0.0,0.0],
                            [0.0,0.0,0.0]])

print("real coordinates:")
print(obj_points[:,:,0])
                            
retval, rvec, tvec = cv2.solvePnP(obj_points, img_points, camera_mat, dist_coeffs)

print("rotation Vector:")
print(rvec[:,0])
print("translation Vector:")
print(tvec[:,0])

rmat = np.zeros((3,3))
cv2.Rodrigues(rvec, rmat, jacobian=0)
print("rotation Matrix:")
print(rmat)

# verify results
print("verify that we get the image points (approximately since given camera parameters are off + numerical instabilities):")
for i in range(img_points.shape[0]):
    imX = img_points[i,0,0]
    imY = img_points[i,1,0]
    
    projected = rmat.dot(obj_points[i,:,:]) + tvec
    x1 = projected[0,0] / projected[2,0]
    y1 = projected[1,0] / projected[2,0]
    r = x1*x1 + y1*y1
    x2 = x1 * (1 + k1 * r + k2 * r * r) + 2 * p1 * x1 * y1 + p2 * (r + 2 * x1 * x1)
    y2 = y1 * (1 + k1 * r + k2 * r * r) + p1 * (r + 2 * y1 * y1) + 2 * p2 * x1 * y1
    u = fx * x2 + cx
    v = fy * y2 + cy
    
    print("actual: (" + str(imX) + ", " + str(imY) + ") computed: (" + str(u) + ", " + str(v) + ")")

# compute homogeneous transform H and the inverse G from the lecture slides
Hmat = np.copy(rmat)
Hmat[:,2] = (- rmat.dot(tvec))[:,0]
Gmat = np.linalg.inv(Hmat) # numerically super unstable but works well in this case
print("homogenous inverse (scaling factor 1):")
print(Gmat)

# get quaternion from rotation matrix
eps0 = math.sqrt(1 + rmat[0,0] + rmat[1,1] + rmat[2,2]) / float(2)
quaternion = np.array([[eps0],[(rmat[2,1] - rmat[1,2]) / (4 * eps0)],[(rmat[0,2] - rmat[2,0]) / (4 * eps0)],[(rmat[1,0] - rmat[0,1]) / (4 * eps0)]])
print("rotation quaternion:")
print(quaternion[:,0])

# yaw pitch and roll
yaw = np.arctan2(rmat[1,0], rmat[0,0])
pitch = np.arctan2(-rmat[2,0], math.sqrt(rmat[2,1] * rmat[2,1] + rmat[2,2] * rmat[2,2]))
roll = np.arctan2(rmat[2,1], rmat[2,2])
print("extrinsic Euler angles as yaw, pitch and roll around the x-axis in degrees:")
print("yaw = " + str(yaw * 180 / math.pi) + " pitch = " + str(pitch * 180 / math.pi) + " roll = " + str(roll * 180 / math.pi))

# camera location
# tvec is the translation within the camera's coordinate frame, that is, if x is the world coordinate, then the camera coordinate is given by c = Rx + t. The camera's position
# in the camera frame is 0 and its real world location x is given by 0 = Rx + t iff x = -R^T * t using that the inverse equals the transpose for rotation matrices
cameraLocation = -rmat.transpose().dot(tvec)
print("Camera location:")
print(cameraLocation[:,0])
