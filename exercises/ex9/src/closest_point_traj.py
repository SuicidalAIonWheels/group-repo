#!/usr/bin/env python
import numpy as np
from math import sqrt

def get_closest_position_on_track(x, y):
    """
    Given some point (x,y), computes the closest point on the oval track
    from exercise 9.
    """
    # define parameters of the track
    lines_min_x = 1.87
    lines_max_x = 4.14
    left_line_y = 0.94
    right_line_y = 3.36
    circle_radius = 1.21
    circle_center_y = (left_line_y + right_line_y) / 2.0
    
    # We can figure out based on the x coordinate which part of the track
    # is closest: If x < lines_min_x then the closest point is on the
    # upper half circle, if x > lines_max_x it is on the lower one.
    # Otherwise x lies within [lines_min_x, lines_max_x] and the closest
    # point is on one of the straight line segments. Note that the mid
    # point of the circles, which would have infinitely many solutions,
    # is taken care of by this.
    if x < lines_min_x:
        # The point we want is the intersection of the line through
        # the circle center C and P=(x,y) with the circle itself.
        # This is given by C + circle_radius * (P-C) / norm(P-C)
        normPC = sqrt((x - lines_min_x)**2 + (y - circle_center_y)**2)
        x_on_circle = lines_min_x + circle_radius * (x - lines_min_x) / normPC
        y_on_circle = circle_center_y + circle_radius * (y - circle_center_y) / normPC
        
        return x_on_circle, y_on_circle
        
    elif x > lines_max_x:
        normPC = sqrt((x - lines_max_x)**2 + (y - circle_center_y)**2)
        x_on_circle = lines_max_x + circle_radius * (x - lines_max_x) / normPC
        y_on_circle = circle_center_y + circle_radius * (y - circle_center_y) / normPC
        
        return x_on_circle, y_on_circle
        
    else:
        distance_left = abs(y - left_line_y)
        distance_right = abs(y - right_line_y)
        if distance_left < distance_right:
            return x, left_line_y
        else:
            return x, right_line_y
            
if __name__ == '__main__':
    print(get_closest_position_on_track(0, 0))
    print(get_closest_position_on_track(2, 4))
    print(get_closest_position_on_track(1, 3))
    print(get_closest_position_on_track(0.66, 2.15))

