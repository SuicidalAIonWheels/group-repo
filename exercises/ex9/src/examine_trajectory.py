#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
from closest_point_traj import get_closest_position_on_track

# load coordinates of the car
coords = np.load("../bagfiles/bag3.npy")

# get nearest points on track
track_coords = np.empty(coords.shape)
distances = np.empty(coords.shape[0])
squared_distances = np.empty(coords.shape[0])
for i in range(coords.shape[0]):
    track_coords[i,:] = get_closest_position_on_track(coords[i,0], coords[i,1])
    squared_distances[i] = (coords[i,0] - track_coords[i, 0])**2 + (coords[i,1] - track_coords[i, 1])**2
    distances[i] = sqrt(squared_distances[i])

plt.plot(coords[:,1], coords[:,0], 'b', track_coords[:,1], track_coords[:,0], 'r')
plt.gca().invert_yaxis()
plt.xlabel('y')
plt.ylabel('x')
plt.legend(['car coordinates', 'closest point on track'])
plt.title("average distance: {:.2f}\naverage squared distance: {:.2f}".format(np.average(distances), np.average(squared_distances)))
plt.show()
