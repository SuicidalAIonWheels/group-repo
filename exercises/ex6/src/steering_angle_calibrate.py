#!/usr/bin/env python

import sys

import numpy as np
import math
import rospy
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Int16, UInt8, UInt16

"""
We failed to get proper measurements for A) finding the walls, but this code should at least solve B through D.
"""

def computeRadius(A, B, C):
    # computes radius of the circle given by the points A, B and C
    # this is just some formula one can find on the internet
    denom = 2 * (A[0] * (B[1] - C[1]) - A[1] * (B[0] - C[0]) + B[0] * C[1] - C[0] * B[1]  )
    centerX = ((A[0]**2 + A[1]**2) * (B[1] - C[1]) + (B[0]**2 + B[1]**2) * (C[1] - A[1]) + (C[0]**2 + C[1]**2) * (A[1] - B[1])) / denom
    centerY = ((A[0]**2 + A[1]**2) * (C[0] - B[0]) + (B[0]**2 + B[1]**2) * (A[0] - C[0]) + (C[0]**2 + C[1]**2) * (B[0] - A[0])) / denom
    # (centerX, centerY) should be the midpoint of the circle
    
    return math.sqrt((centerX - A[0])**2 + (centerY - A[1])**2 ) # radius
    
def computeSteeringAngle(R, axisDistance):
    # computes the steering angle assuming the lidar sits on top of the rear axis
    # (and after all, we only know the circle the lidar travelled along)
    # takes radius R as computed by computeRadius() and the axis distance we forgot to measure out of frustration
    
    return math.atan2(axisDistance, R) # using atan2 ensures correct sign of the steering angle in radians
    
def getCommandAngleFromSteeringAngle(steeringAngle):
    # maps steering angles to command angles from 0 to 179 using measurements
    
    # get correspondences we've measured
    # optimally we would read this from a file but all measurements we actually got were worthless, so a hardcoded one has to suffice
    commandAngles = [0, 30, 60, 90, 120, 150, 179]
    measuredAngles = [0.772888447144, 0.664701039451, 0.314997696704, 0.0357352441532, -0.113714309545, -0.494136109243, -0.772888447144]
    # (this is the best we obtained using the auxiliary script from the exercise sheet repo)
    # (for 179 we actually have NaN in the measurement so it's minus the one for 0 here)
    
    intervalFound = False
    commandAngle = -1
    for i in range(len(measuredAngles) - 1):
        if measuredAngles[i] >= steeringAngle and measuredAngles[i + 1] <= steeringAngle:
            # we know that whatever we return has to be between commandAngles[i] and commandAngles[i+1]
            # for simplicity we use a linear mapping of the measured angle interval to the command angle interval
            commandAngle = commandAngles[i] + (steeringAngle - measuredAngles[i + 1]) * (commandAngles[i+1] - commandAngles[i]) / (measuredAngles[i] - measuredAngles[i + 1])
            intervalFound = True
            break
            
    if not intervalFound:
        raise Exception("Cannot map steering angle, out of measured bounds!")
        
    return commandAngle
########
def save_xml(command,raduis,steering,feedback):
	
	file ="SteerAngleActuator.xml"
	tree = xml.etree.ElementTree.parse(file)
	root = tree.getroot().iter("myPair")
	for child in root:
		if child.tag== 'myPair':
			root2=child
			found = False
			for child2 in root2:
				if child2.tag == 'item':
					for child3 in child2:
						if child3.tag=='command' and child3.text==str(command):
							for child4 in child2:
								if child4.tag=='raduis':
									child4.text=str(raduis)
								elif child4.tag=='steering':
									child4.text=str(steering)
								elif child4.tag=='feedback':
									child4.text=str(feedback)
							print("save_xml: item is found")
							found = True
					if (found):
						break
							
	tree.write(file) 
def steering_feedback_callback(steering_angle):
	global steering_angle_feedback
	steering_angle_feedback=int(steering_angle.data)
def get_distance(points, slope, intercept):
	""" return the distance for each point to the parametrised line """
	pos = np.array((0, intercept))  # origin
	dir = np.array((1, slope))  # line gradient
	# element-wise cross product of each points origin offset with the gradient
	c = np.cross(dir, pos - points, axisb=-1)
	return np.abs(c) / np.linalg.norm(dir)
def stop_driving():
	pub_speed.publish(0)
	rospy.sleep(1)
	rospy.signal_shutdown('stop')
	print('stop driving')
	print ('close the plot to stop the program!')


def get_inliers(points, slope, intercept):
	""" return a numpy boolean array for each point (True if within 'inlier_dist' of the line, else False). """
	return get_distance(points, slope, intercept) <= inlier_dist

def driveAndScan:
	speed = 150
	steering_angle = 0
	driveDurationMax = 5
	pub_stop_start = rospy.Publisher("/manual_control/stop_start",Int16, queue_size=100, latch=True)
	pub_speed = rospy.Publisher("/speed", Int16, queue_size=100,latch=True)
	pub_steering = rospy.Publisher("/steering", UInt8, queue_size=100,latch=True)
	
def getPoints(angleOne,angleTwo):
	radius = np.asarray(scan_msg.ranges)
	angles = np.arange(scan_msg.angle_min, scan_msg.angle_max + scan_msg.angle_increment / 2, scan_msg.angle_increment)

	mask_fin = np.isfinite(radius)  # only consider finite radii

	if mask_angles:
		if (abs(target_angle-wall_angle)<np.pi/4):
			target_angle = wall_angle # right side of the car
		angle_spread = np.pi / 4  # size of cone to consider
		#if Lidar installed inversed!
		#mask_angle = np.logical_or((-np.pi+target_angle + angle_spread) > angles, angles > (np.pi+target_angle - angle_spread))
		#if Lidar installed direct!
		mask_angle = np.logical_or((target_angle + angle_spread) > angles, angles > (target_angle - angle_spread))

		mask = np.logical_and(mask_fin, mask_angle)
	else:
		mask = mask_fin

	masked_angles = angles[mask]
	masked_radius = radius[mask]

	# calculate coordinates of our masked values
	x = np.cos(masked_angles) * masked_radius
	y = np.sin(masked_angles) * masked_radius

	X = np.ones((np.size(x),3))
	X[:,0]=x
	X[:,2]=y

	points = np.column_stack((x, y))
	
def computeAll(A,B,C):

def RANSAClinefit(self, points, maxNumIter, maxDistance, requiredInlierPercentage):
        iteration = 0
        fitThesePoints = None
        d = requiredInlierPercentage * points.shape[0]
        
        while iteration < maxNumIter:
            firstPointIndex = np.random.randint(0, points.shape[0])
            secondPointIndex = np.random.randint(0, points.shape[0])
                
            firstPoint = points[firstPointIndex, :]
            secondPoint = points[secondPointIndex, :]
            
            # fit line to the two selected points
            if firstPoint[0] != secondPoint[0]: # ignore bad pair
                m = (firstPoint[1] - secondPoint[1]) / float(firstPoint[0] - secondPoint[0])
                b = firstPoint[1] - m * firstPoint[0]
            
                # determine if we have enough inliers
                inliers = []
                for i in range(points.shape[0]):
                    candidate = points[i,:]
                    distance = abs(m * candidate[0] - candidate[1] + b) / math.sqrt(m * m + 1)
                    if distance < maxDistance:
                        inliers.append(candidate)
                
                if len(inliers) >= d:
                    fitThesePoints = np.array(inliers)
                    break
                
        if fitThesePoints is None:
            raise Exception('Unable to find good fit!')
            
        return cv2.fitLine(fitThesePoints, cv2.DIST_L2, 0, 0.01, 0.01)

def main(args):
	global steering_angle
	rospy.init_node("angle_calibration")
	if len(args) > 1:
		try:
			steering_angle = int(args[1])
			rospy.Subscriber("/scan", LaserScan, scan_callback, queue_size=1)
			rospy.Subscriber("/steering_angle", UInt16, steering_feedback_callback, queue_size=1)  # small queue for only reading recent data

		except rospy.ROSInterruptException:
			pass
	else:
		print("please provide a steering setting from [0,180]") 

	if plotting:
		plt.show()  # block until plots are closed
	else:
		rospy.spin()

if __name__ == '__main__':
	main(sys.argv)
