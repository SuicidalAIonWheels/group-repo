#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
import sys
sys.path.insert(0, '../../../src/assignment11/src')
from predict_closest_point import get_closest_point

# load coordinates of the car
coords = np.load("../location_logs/location_log.npy")

# get nearest points on track
track1_coords = np.empty(coords.shape)
track2_coords = np.empty(coords.shape)
for i in range(coords.shape[0]):
    track1_coords[i,:] = get_closest_point(coords[i,:], 1, 0.0)
    track2_coords[i,:] = get_closest_point(coords[i,:], 2, 0.0)

plt.plot(coords[:,1], coords[:,0], 'b', track1_coords[:,1], track1_coords[:,0], 'r', track2_coords[:,1], track2_coords[:,0], 'r')
plt.gca().invert_yaxis()
plt.xlabel('y')
plt.ylabel('x')
plt.legend(['car coordinates', 'closest point on lane 1', 'closest point on lane 2'])
plt.show()
