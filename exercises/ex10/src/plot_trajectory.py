#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
import sys
sys.path.insert(0, '../../../src/assignment10/src')
from predict_closest_point import get_closest_point

# load coordinates of the car
coords = np.load("../location_logs/location_log.npy")

# get nearest points on track
track_coords = np.empty(coords.shape)
squared_distances = np.empty(coords.shape[0])
for i in range(coords.shape[0]):
    track_coords[i,:] = get_closest_point(coords[i,:], 1, 0.0)
    squared_distances[i] = (coords[i,0] - track_coords[i, 0])**2 + (coords[i,1] - track_coords[i, 1])**2

plt.plot(coords[:,1], coords[:,0], 'b', track_coords[:,1], track_coords[:,0], 'r')
plt.gca().invert_yaxis()
plt.xlabel('y')
plt.ylabel('x')
plt.legend(['car coordinates', 'closest point on track'])
plt.title("average squared distance: {:.6f}".format(np.average(squared_distances)))
plt.show()
